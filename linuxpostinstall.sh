#!/bin/bash
#Postinstallation script for various linux distros
#Make sure to run this script as root!
input=""

# Detect OS
function osDetection() {
	DISTRO=$(cat /etc/*-release | grep -w NAME | cut -d= -f2 | tr -d '"')
	if [[ "$DISTRO" == "Ubuntu" ]]
		then
      echo "###################################################"
    	echo "Determined platform: $DISTRO"
    	echo "###################################################"
      sleep 1
	elif [[ "$DISTRO" == "Fedora" ]]
		then
			if [ -x "$(command -v dnf)" ];
        then
          echo "###################################################"
        	echo "Determined platform: $DISTRO"
        	echo "###################################################"
          sleep 1
      else
        echo "###################################################"
      	echo "Determined platform: Fedora Silverblue"
      	echo "###################################################"
        sleep 1
        startPostscript "Fedora Silverblue" "fedora/fedoraSilverblue/fedorasilverblue.sh"
      fi
  elif [[ "$DISTRO" == "Pop!_OS" ]]
    then
      echo "###################################################"
    	echo "Determined platform: $DISTRO"
    	echo "###################################################"
			startPostscript "Pop!_OS" "debian/ubuntu/popos/popos.sh"
      sleep 1
  else
		echo "###################################################"
 		echo "OS NOT DETECTED, couldn't install package "
		echo "###################################################"
 	  exit 1;
	fi
}

#Starts the Postinstallation script for each distro
function startPostscript() {
  clear
  echo "###################################################"
  echo "Start $1 script [y/n/]?"
  echo "###################################################"
  echo
  read input
  case $input in
      [yY] )	chmod +x $2
              $2;;
      * )	echo "###################################################"
          echo "Beenden des Scriptes..."
          echo "###################################################"
          echo
          exit 1;;
    esac
}

#Start
clear
osDetection
