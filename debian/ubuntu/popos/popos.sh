#!/bin/bash
##### Script to install my basic configuration for Pop OS
input=""
debug=false

#setting the path to the user directory
home=$(getent passwd $USER | cut -d: -f6)

#function to set up the debug mode = never clearing the terminal
function debugMode() {
  echo "###################################################"
  echo "Do you want to use debug mode [y/n]?"
  echo "###################################################"
  echo
  read input
  case $input in
    [yY] )	echo
            echo "###################################################"
            echo "Setting debug mode"
            echo "###################################################"
            debug=true
            sleep 1;;
    * )     echo
            echo "###################################################"
            echo "Debug mode is not gonna be used"
            echo "###################################################"
            sleep 1
  esac
}

#Set hostname function
function hostname() {
  if [[ $debug == false ]];
    then
      clear
  fi
  echo "###################################################"
  echo "Please enter a Hostname for your System"
  echo "###################################################"
  echo
  read input
  hostnamectl set-hostname $input
  echo
  echo "###################################################"
  echo "Hostname is now " $input
  echo "###################################################"
  sleep 1
}

#setting up the gnome desktop
function gnomeSettings() {
  if [[ $debug == false ]];
    then
      clear
  fi
  echo "###################################################"
  echo "Setting up your Gnome Desktop"
  echo "###################################################"


  gsettings set org.gnome.desktop.calendar show-weekdate true

  echo "Keep Pop-Theme [y/n]?"
  read input
  case $input in
      [yY] )	echo "Disable Darkmode [y/n]?"
              read input
              case $input in
                [yY] )	gsettings set org.gnome.desktop.interface gtk-theme Pop;;
                * )		  gsettings set org.gnome.desktop.interface gtk-theme Pop-dark;;
              esac;;
      [nN] )  echo "Disable Darkmode [y/n]?"
              read input
              case $input in
                [yY] )	gsettings set org.gnome.desktop.interface gtk-theme Adwaita
                        gsettings set org.gnome.desktop.interface icon-theme 'Adwita';;
                * )		  gsettings set org.gnome.desktop.interface gtk-theme Adwaita-dark
                        gsettings set org.gnome.desktop.interface icon-theme 'Adwita';;
              esac;;
      * )		  echo "Disable Darkmode [y/n]?"
              read input
              case $input in
                [yY] )	gsettings set org.gnome.desktop.interface gtk-theme Pop;;
                * )		  gsettings set org.gnome.desktop.interface gtk-theme Pop-dark;;
              esac;;
  esac

  gsettings set org.gnome.desktop.interface clock-format 24h

  echo "Disable animations [y/n]?"
  read input
  case $input in
      [yY] )	gsettings set org.gnome.desktop.interface enable-animations false;;
      * )		  gsettings set org.gnome.desktop.interface enable-animations true;;
  esac

  echo "Disable Hot Corner [y/n]?"
  read input
  case $input in
      [yY] )	gsettings set org.gnome.desktop.interface enable-hot-corners false;;
      * )		  gsettings set org.gnome.desktop.interface enable-hot-corners true;;
  esac

  gsettings set org.gnome.desktop.interface clock-show-weekday true
  gsettings set org.gnome.desktop.interface show-battery-percentage true
  gsettings set org.gnome.desktop.privacy hide-identity true
  gsettings set org.gnome.desktop.privacy report-technical-problems false
  gsettings set org.gnome.desktop.privacy send-software-usage-stats false
  gsettings set org.gnome.desktop.privacy recent-files-max-age 30
  gsettings set org.gnome.desktop.privacy remove-old-trash-files true
  gsettings set org.gnome.desktop.privacy remove-old-temp-files true
  gsettings set org.gnome.desktop.privacy old-files-age 30
  gsettings set org.gnome.desktop.screensaver idle-activation-enabled true
  gsettings set org.gnome.desktop.screensaver lock-enabled true
  gsettings set org.gnome.desktop.screensaver lock-delay 0
  gsettings set org.gnome.desktop.screensaver status-message-enabled false
  gsettings set org.gnome.desktop.screensaver user-switch-enabled false
  gsettings set org.gnome.desktop.notifications show-in-lock-screen false
  gsettings set org.gnome.mutter attach-modal-dialogs true

  echo "Allow Volume above 100% [y/n]?"
  read input
  case $input in
      [yY] )	gsettings set org.gnome.desktop.sound allow-volume-above-100-percent true;;
      * )		  gsettings set org.gnome.desktop.sound allow-volume-above-100-percent false;;
  esac

  echo "Disable system sounds [y/n]?"
  read input
  case $input in
      [yY] )	gsettings set org.gnome.desktop.sound event-sounds false;;
      * )		  gsettings set org.gnome.desktop.sound event-sounds true;;
  esac

  echo "Enable nightlight [y/n]?"
  read input
  case $input in
      [yY] )	gsettings set org.gnome.settings-daemon.plugins.color night-light-enabled true;;
      * )		  gsettings set org.gnome.settings-daemon.plugins.color night-light-enabled false;;
  esac

  echo "Enable minimize and maximize buttons [y/n]?"
  read input
  case $input in
      [yY] )	gsettings set org.gnome.desktop.wm.preferences button-layout 'appmenu:minimize,maximize,close';;
      * )		  gsettings set org.gnome.desktop.wm.preferences button-layout 'appmenu:close';;
  esac

  echo "Enable neo2 keyboard layout [y/n]?"
  read input
  case $input in
      [yY] )	gsettings set org.gnome.desktop.input-sources sources "[('xkb', 'de+neo'), ('xkb', 'de')]";;
      * )		  gsettings set org.gnome.desktop.input-sources sources "[('xkb', 'de')]";;
  esac

  echo "Disable Desktop Icons [y/n]?"
  read input
  case $input in
      [yY] )	gsettings set org.gnome.shell disabled-extensions "['ding@rastersoft.com']";;
      * )		  gsettings set org.gnome.shell enabled-extensions "['alt-tab-raise-first-window@system76.com', 'always-show-workspaces@system76.com', 'pop-shell@system76.com', 'pop-shop-details@system76.com', 'system76-power@system76.com', 'ubuntu-appindicators@ubuntu.com', 'ding@rastersoft.com']";;
  esac


  #settings for nautilus
  gsettings set org.gtk.Settings.FileChooser sort-directories-first true
  gsettings set org.gnome.nautilus.list-view use-tree-view true
  gsettings set org.gnome.nautilus.preferences show-create-link true
  gsettings set org.gnome.nautilus.preferences executable-text-activation "'ask'"
  gsettings set org.gnome.nautilus.icon-view  captions  "['size', 'permissions', 'none']"

  #settings for system processes
  gsettings set org.gnome.gnome-system-monitor show-whose-processes "'active'"


  #settings for calculator
  gsettings set org.gnome.calculator show-thousands true

  #settings for gedit
  gsettings set org.gnome.gedit.preferences.editor highlight-current-line false
  gsettings set org.gnome.gedit.preferences.editor auto-save true
  gsettings set org.gnome.gedit.plugins active-plugins "['spell', 'filebrowser', 'modelines', 'docinfo', 'time', 'sort']"

  #settings for eog
  gsettings set org.gnome.eog.ui statusbar true

  #settings for fileroller
  gsettings set org.gnome.FileRoller.UI view-sidebar true



  #turning off Fedora Logo
  gsettings set org.gnome.shell enabled-extensions "[]"
  sleep 1
}

#setting up importand keybindings for the gnome desktop
function keybindings() {
  if [[ $debug == false ]];
    then
      clear
  fi
  echo "###################################################"
  echo "Setting importand keybindings"
  echo "###################################################"
  echo
    gsettings set org.gnome.desktop.wm.keybindings close "['<Super>q']"
    gsettings set org.gnome.desktop.wm.keybindings toggle-on-all-workspaces "['<Primary><Shift>a']"
    gsettings set org.gnome.desktop.wm.keybindings toggle-fullscreen "['F11']"
    gsettings set org.gnome.settings-daemon.plugins.media-keys www "[]"
    gsettings set org.gnome.desktop.wm.keybindings show-desktop "['<Super>d']"
    gsettings set org.gnome.settings-daemon.plugins.media-keys screenshot "['<Super>Print']"
    gsettings set org.gnome.settings-daemon.plugins.media-keys control-center "['<Super>i']"
    gsettings set org.gnome.settings-daemon.plugins.media-keys home "['<Super>f']"
    gsettings set org.gnome.settings-daemon.plugins.media-keys play "['<Primary>space']"

  #Custom keybindings
    gsettings set org.gnome.settings-daemon.plugins.media-keys custom-keybindings "['/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/', '/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom1/', '/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom2/', '/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom3/']"
    gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/ name "Terminal"
    gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/ command "gnome-terminal"
    gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/ binding "<Super>t"

    gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom1/ name "Tor-Browser"
    gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom1/ command "flatpak run com.github.micahflee.torbrowser-launcher"
    gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom1/ binding "<Super>b"

    gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom2/ name "Atom"
    gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom2/ command "flatpak run io.atom.Atom"
    gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom2/ binding "<Super>c"

    gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom3/ name "KeePassXC"
    gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom3/ command "flatpak run org.keepassxc.KeePassXC"
    gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom3/ binding "<Super>k"
  sleep 1
}

#Update the System
function updates() {
  if [[ $debug == false ]];
    then
      clear
  fi
  echo "###################################################"
  echo "Updating your drivers"
  echo "###################################################"
  echo
  fwupdmgr get-devices
  fwupdmgr refresh --force
  fwupdmgr get-updates
  fwupdmgr update
  sleep 1
}

#Flatpak Install function
function flatpakInstall() {
  if [[ $debug == false ]];
    then
      clear
  fi
  echo "###################################################"
  echo "Installing " $1
  echo "###################################################"
	flatpak install flathub "$2" -y;
  error=$( echo $? )
	if [[ $error -ne 0 ]];
    then
      echo;
      echo "Fehler beim Installieren von ""\"$appname\"";
      echo;
      echo;
  fi
  echo
  sleep 1
}

#Flatpak stall function with user action required
function flatpakInstallAction() {
  if [[ $debug == false ]];
    then
      clear
  fi
  6 "Chromium Browser" on
  7 "Evolution" on
  8 "Thunderbird" off
  9 "Signal Desktop" on
  10 "Element" on
  11 "Microsoft Teams" off
  18 "Gnome Karten" on
  19 "Remmina" off
  23 "Virt-Manager" off
  27 "Nextcloud Sync" on
  29 "GTK Hash" on
  30 "VeraCrypt" on
  32 "Nitrokey" on
}



clear
cd $home
debugMode
hostname
gnomeSettings
keybindings
updates
sudo chmod +x linux-postinstallation-scripts/debian/ubuntu/popos/poposroot.sh
sudo linux-postinstallation-scripts/debian/ubuntu/popos/poposroot.sh

flatpak update -y
flatpakInstall "Kooha (Screenrecorder)" io.github.seadve.Kooha
flatpakInstall Tor com.github.micahflee.torbrowser-launcher
flatpakInstall KeePassXC org.keepassxc.KeePassXC
