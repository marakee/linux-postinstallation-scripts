#!/bin/bash
##### It must be run as root or with sudo privileges

#Setting user to the actual user and not to root
user=$SUDO_USER
#setting the path to the user directory
home=$(getent passwd $SUDO_USER | cut -d: -f6)
#root detection
function rootDetection() {
	if [[ $EUID -ne 0 ]]
		then
			echo "###################################################"
	  	echo "Führe dieses Script bitte als root aus!" 1>&2
	  	echo "Beenden des Scriptes..."
			echo "###################################################"
	  	echo
	  	exit 1
	fi
}

function updates() {
  if [[ $debug == false ]];
    then
      clear
  fi
  echo "###################################################"
  echo "Updating your system and apps"
  echo "###################################################"
  echo
  sudo apt update -y
  sudo apt upgrade -y
  sudo apt dist-upgrade -y
  sudo apt autoremove -y
  sudo apt autoclean -y
  sleep 1
}

function musthave() {
	apt install ubuntu-restricted-extras -y
	apt install gufw -y
	ufw enable

}

function installer() {
	apt-get install dialog
	cmd=(dialog --separate-output --checklist "Please Select Software you want to install:" 22 76 16)
	options=(
					 36 "TLP" on
					 38 "USBGuard" on
					 37 "Tilix" off
					 21 "KeePass2" on
					 24 "Déjà Dup Datensicherungen" on
           25 "Timeshift" off
           26 "GParted" off
					 34 "Wine" off
					 40 "Apostrophe" on
           3 "Atom" on
           4 "Visual Studio Code" off
					 3.1 "Latex" on
           12 "Shotwell" on
           13 "Gimp" off
           14 "Lollypop" on
           16 "EasyTag" on
           17 "VLC Media Player" on
           22 "Gnome Boxen" on
           )

  choices=$("${cmd[@]}" "${options[@]}" 2>&1 >/dev/tty)
  clear
  for choice in $choices
  do
      case $choice in
          1)
                #Ubuntu Restricted Extras
                echo "Installiere Ubuntu Restricted Extras"
                sudo apt install ubuntu-restricted-extras -y
                ;;
          2)
                #Gnome tweak tool
                echo "Installiere Gnome Tweak Tool"
                sudo apt install gnome-tweaks -y
                ;;
          2.1)
                #Dash to Dock
                echo "Installiere Dash to Dock"
                sudo apt install gnome-shell-extension-dashtodock -y
                ;;
          2.2)
                #GSConnect
                echo "Installiere GSConnect"
                sudo apt install gnome-shell-extension-gsconnect -y
                ;;
          2.3)
                #Bluetooth Quick Connect
                echo "Installiere Bluetooth Quick Connect"
                sudo apt install gnome-shell-extension-bluetooth-quick-connect -y
                ;;
          2.4)
                #Disconnect Wifi
                echo "Installiere Disconnect Wifi"
                sudo apt install gnome-shell-extension-disconnect-wifi -y
                ;;
          2.5)
                #Multi-Monitors
                echo "Installiere Multi-Monitors"
                sudo apt install gnome-shell-extension-multi-monitors -y
                ;;
          2.6)
                #No Annoyance
                echo "Installiere No Annoyance"
                sudo apt install gnome-shell-extension-no-annoyance -y
                ;;
          3)
                #Atom editor
                echo "Installiere Atom"
                sudo apt install atom -y
                apm install pdf-view
                apm install atom-live-server
                apm install emmet
                ;;
          3.1)
                #Latex
                echo "Installiere Latex"
                sudo apt install texlive-full -y
                apm install latex
                apm install language-latex
                ;;
          4)
                #VSCode editor
                echo "Installiere Visual Studio Code"
                sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
                sudo flatpak install flathub com.visualstudio.code -y
                ;;
          5)
                #Tor Browser
                echo "Installiere Tor Browser"
                sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
                sudo flatpak install flathub com.github.micahflee.torbrowser-launcher -y
                ;;
          6)
                #Chromium Browser
                echo "Installiere Chromium Browser"
                sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
                sudo flatpak install flathub org.chromium.Chromium -y
                ;;
          7)
                #Evolution
                echo "Installiere Evolution"
                sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
                sudo flatpak install flathub org.gnome.Evolution -y
                gsettings set org.gnome.evolution-data-server.calendar contacts-reminder-enabled true
                ;;
          8)
                #Thunderbird
                echo "Installiere Thunderbird"
                sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
                sudo flatpak install flathub org.mozilla.Thunderbird -y
                ;;
          9)
                #Signal Desktop
                echo "Installiere Signal Desktop"
                sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
                sudo flatpak install flathub org.signal.Signal -y
                ;;
          10)
                #Element
                echo "Installiere Element"
                sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
                sudo flatpak install flathub im.riot.Riot -y
                ;;
          11)
                #Microsoft Teams
                echo "Installiere Microsoft Teams"
                sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
                sudo flatpak install flathub com.microsoft.Teams -y
                ;;
          12)
                #Shotwell
                echo "Installiere Shotwell"
                sudo apt install shotwell -y
                gsettings set org.yorba.shotwell.plugins.enable-state org-gnome-shotwell-publishing-google-photos false
                gsettings set org.yorba.shotwell.plugins.enable-state publishing-facebook false
                gsettings set org.yorba.shotwell.plugins.enable-state publishing-flickr false
                gsettings set org.yorba.shotwell.plugins.enable-state publishing-piwigo false
                gsettings set org.yorba.shotwell.plugins.enable-state publishing-youtube false
                gsettings set org.yorba.shotwell.preferences.ui events-sort-ascending true
                ;;
          13)
                #Gimp
                echo "Installiere Gimp"
                sudo apt install gimp -y
                ;;
          14)
                #Lollypop
                echo "Installiere Lollypop"
                sudo apt install lollypop -y
                sudo apt remove mpv -y
                gsettings set org.gnome.Lollypop allow-per-track-cover true
                gsettings set org.gnome.Lollypop background-mode true
                gsettings set org.gnome.Lollypop fade-duration 10000
                gsettings set org.gnome.Lollypop save-state true
                gsettings set org.gnome.Lollypop transitions-duration 10000
                ;;

          15)
                #Audacity
                echo "Installiere Audacity"
                sudo apt install audacity -y
                ;;

          16)
                #EasyTag
                echo "Installiere EasyTag"
                sudo apt install easytag -y
                gsettings set org.gnome.EasyTAG rename-file-default-mask "%n - %t - %a - %b"
                ;;
          17)
                #VLC
                echo "Installiere VLC Media Player"
                sudo apt install vlc -y
                ;;
          18)
                #Gnome Karten
                echo "Installiere Gnome Karten"
                sudo apt install gnome-maps -y
                gsettings set org.gnome.Maps hybrid-aerial true
                gsettings set org.gnome.Maps transportation-type car
                ;;
          19)
                #Remmina
                echo "Installiere Remmina"
                sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
                sudo flatpak install flathub org.remmina.Remmina -y
                ;;
          20)
                #KeePassXC
                echo "Installiere KeePassXC"
                sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
                sudo flatpak install flathub org.keepassxc.KeePassXC -y
                ;;
          21)
                #KeePass2
                echo "Installiere KeePass2"
                sudo apt install keepass2 -y
                ;;
          22)
                #Gnome Boxen
                echo "Installiere Gnome Boxen"
                sudo apt install gnome-boxes -y
                ;;
          23)
                #Virt-Manager
                echo "Installiere Gnome Boxen"
                sudo apt install virt-manager -y
                ;;
          24)
                #Déjà Dup Datensicherungen
                echo "Installiere Déjà Dup Datensicherungen"
                sudo apt install deja-dup -y
                gsettings set org.gnome.DejaDup backend local
                ;;
          25)
                #Virt-Manager
                echo "Installiere Timeshift"
                sudo apt install timeshift -y
                gsettings set org.virt-manager.virt-manager system-tray true
                ;;
          26)
                #GParted
                echo "Installiere GParted"
                sudo apt install gparted -y
                ;;
          27)
                #Nextcloud Sync
                echo "Installiere Nextcloud Sync"
                sudo apt install nextcloud-desktop -y
                ;;
          28)
                #Firewall
                echo "Installiere Firewall"
                sudo apt install gufw -y
                sudo ufw enable
                ;;
          29)
                #GTK Hash
                echo "Installiere GTK Hash"
                sudo apt install gtkhash -y
                ;;
          30)
                #VeraCrypt
                echo "Installiere VeraCrypt"
                wget https://launchpad.net/veracrypt/trunk/1.24-update7/+download/veracrypt-1.24-Update7-Ubuntu-20.10-amd64.deb
                sudo dpkg -i veracrypt-1.24-Update7-Ubuntu-20.10-amd64.deb
                sudo apt-get install -f -y
                sudo rm -rf veracrypt-1.24-Update7-Ubuntu-20.10-amd64.deb
                ;;
          31)
                #Teamviewer
                echo "Installing Teamviewer"
                wget http://download.teamviewer.com/download/teamviewer_i386.deb
                sudo dpkg -i teamviewer_i386.deb
                sudo apt-get install -f -y
                sudo rm -rf teamviewer_i386.deb
                ;;
          32)
                #Nitrokey
                echo "Installiere Nitrokey"
                sudo apt-get install libccid -y
                sudo apt install nitrokey-app -y
                ;;
          33)
                #Joplin
                echo "Installiere Joplin"
                wget -O - https://raw.githubusercontent.com/laurent22/joplin/dev/Joplin_install_and_update.sh | bash
                ;;
          34)
                #Wine
                echo "Installiere Wine"
                sudo apt install wine -y
                ;;
          35)
                #Dconf
                echo "Installiere Dconf"
                sudo apt install dconf-editor -y
                ;;
          36)
                #TLP
                echo "Installiere TLP"
                sudo apt-get install tlp -y
                sudo tlp start
                ;;
          37)
                #Tilix
                echo "Installiere Tilix"
                sudo apt-get install tilix -y
                ;;
          38)
                #USBGuard
                echo "Installiere USBGuard"
                sudo apt-get install usbguard -y
                ;;
          39)
                #Software-Property-GTK
                echo "Installiere Software-Property-GTK"
                sudo apt install software-properties-gtk -y
                ;;
          40)
                #Apostrophe
                echo "Installiere Apostrophe"
                sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
                flatpak install flathub org.gnome.gitlab.somas.Apostrophe -y
                ;;
      esac
}

rootDetection
updates
musthave
installer
