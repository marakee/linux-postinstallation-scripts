#!/bin/bash
##### Script to install my basic configuration for Fedora Silverblue
input=""
debug=false

#setting the path to the user directory
home=$(getent passwd $USER | cut -d: -f6)
#getting the user of the user
#RUID=$(who | awk 'FNR == 1 {print $1}')
#RUSER_UID=$(id -u ${RUID})

#Update the System
function updates() {
  if [[ $debug == false ]];
    then
      clear
  fi
  echo "###################################################"
  echo "Updating your system and apps"
  echo "###################################################"
  echo
  rpm-ostree upgrade
  error=$( echo $? )
	if [[ $error -ne 0 ]];
    then
      echo
      sek=60
      while [ $sek -ge 1 ]
      do
         echo -en "\rFehler beim Update vom System! Wiederhole in $sek Seunden erneut."
         sleep 1
         sek=$[$sek-1]
      done
      echo
      updates
  fi
  echo
  sleep 1
  fwupdmgr get-devices
  fwupdmgr refresh --force
  fwupdmgr get-updates
  fwupdmgr update
  flatpak update -y
  sleep 1
}

#Install function
function flatpakInstall() {
  if [[ $debug == false ]];
    then
      clear
  fi
  echo "###################################################"
  echo "Installing " $1
  echo "###################################################"
	flatpak install flathub "$2" -y;
  error=$( echo $? )
	if [[ $error -ne 0 ]];
    then
      echo;
      echo "Fehler beim Installieren von ""\"$appname\"";
      echo;
      echo;
  fi
  echo
  sleep 1
}

#Install function with user action required
function flatpakInstallAction() {
  if [[ $debug == false ]];
    then
      clear
  fi
  echo "###################################################"
  echo "Do you want to install" $1 "[y/n]?"
  echo "###################################################"
  echo
  read input
  case $input in
    [yY] )	echo
            echo "###################################################"
            echo "Installing " $1
            echo "###################################################"
          	flatpak install flathub "$2" -y;
            error=$( echo $? )
          	if [[ $error -ne 0 ]];
              then
                echo;
                echo "Fehler beim Installieren von ""\"$1\"";
                echo;
                echo;
            fi
            echo
            sleep 1;;
    * )     echo
            echo "###################################################"
            echo $1 "is not gonna be installed!"
            echo "###################################################"
            sleep 1
  esac
}

#Non-Flatpak install functionn
function rpmInstall() {
  if [[ $debug == false ]];
    then
      clear
  fi
  echo "###################################################"
  echo "Are you sure you want to install $1 [y/n]? It is a non flatpak app and can cause trouble with updates and should only be used if there is no alternativ left."
  echo "###################################################"
  echo
  read input
  case $input in
    [yY] )	echo
            echo "###################################################"
            echo "Installing " $1
            echo "###################################################"
          	rpm-ostree install "$2";
            error=$( echo $? )
          	if [[ $error -ne 0 ]];
              then
                echo;
                echo "Fehler beim Installieren von ""\"$1\"";
                echo;
                echo;
            fi
            echo
            sleep 1;;
    [nN])	  echo
            echo "###################################################"
            echo $1 "is not gonna be installed!"
            echo "###################################################"
            sleep 1;;
    * )     echo
            echo "###################################################"
            echo $1 "is not gonna be installed!"
            echo "###################################################"
            sleep 1
  esac
}

#Set hostname function
function hostname() {
  if [[ $debug == false ]];
    then
      clear
  fi
  echo "###################################################"
  echo "Please enter a Hostname for your System"
  echo "###################################################"
  echo
  read input
  hostnamectl set-hostname $input
  echo
  echo "###################################################"
  echo "Hostname is now " $input
  echo "###################################################"
  sleep 1
}

#Import Firefox Profile
function firefox() {
  if [[ $debug == false ]];
    then
      clear
  fi
  echo "###################################################"
  echo "Import Firefox Profil"
  echo "###################################################"
  echo
  tar -xvf linux-postinstallation-scripts/firefoxProfiles/firefoxraphael.tar.gz
}

#setting up the gnome desktop
function gnomeSettings() {
  if [[ $debug == false ]];
    then
      clear
  fi
  echo "###################################################"
  echo "Setting up your Gnome Desktop"
  echo "###################################################"


  gsettings set org.gnome.desktop.calendar show-weekdate true

  echo "Enable Darkmode [y/n]?"
  read input
  case $input in
      [yY] )	gsettings set org.gnome.desktop.interface gtk-theme Adwaita-dark;;
      * )		  gsettings set org.gnome.desktop.interface gtk-theme Adwaita;;
  esac

  gsettings set org.gnome.desktop.interface clock-format 24h

  echo "Disable animations [y/n]?"
  read input
  case $input in
      [yY] )	gsettings set org.gnome.desktop.interface enable-animations false;;
      * )		  gsettings set org.gnome.desktop.interface enable-animations true;;
  esac

  echo "Disable Hot Corner [y/n]?"
  read input
  case $input in
      [yY] )	gsettings set org.gnome.desktop.interface enable-hot-corners false;;
      * )		  gsettings set org.gnome.desktop.interface enable-hot-corners true;;
  esac

  gsettings set org.gnome.desktop.interface clock-show-weekday true
  gsettings set org.gnome.desktop.interface show-battery-percentage true
  gsettings set org.gnome.desktop.privacy hide-identity true
  gsettings set org.gnome.desktop.privacy report-technical-problems false
  gsettings set org.gnome.desktop.privacy send-software-usage-stats false
  gsettings set org.gnome.desktop.privacy recent-files-max-age 30
  gsettings set org.gnome.desktop.privacy remove-old-trash-files true
  gsettings set org.gnome.desktop.privacy remove-old-temp-files true
  gsettings set org.gnome.desktop.privacy old-files-age 30
  gsettings set org.gnome.desktop.screensaver idle-activation-enabled true
  gsettings set org.gnome.desktop.screensaver lock-enabled true
  gsettings set org.gnome.desktop.screensaver lock-delay 0
  gsettings set org.gnome.desktop.screensaver status-message-enabled false
  gsettings set org.gnome.desktop.screensaver user-switch-enabled false
  gsettings set org.gnome.desktop.notifications show-in-lock-screen false
  gsettings set org.gnome.mutter attach-modal-dialogs true

  echo "Allow Volume above 100% [y/n]?"
  read input
  case $input in
      [yY] )	gsettings set org.gnome.desktop.sound allow-volume-above-100-percent true;;
      * )		  gsettings set org.gnome.desktop.sound allow-volume-above-100-percent false;;
  esac

  echo "Disable system sounds [y/n]?"
  read input
  case $input in
      [yY] )	gsettings set org.gnome.desktop.sound event-sounds false;;
      * )		  gsettings set org.gnome.desktop.sound event-sounds true;;
  esac

  echo "Enable nightlight [y/n]?"
  read input
  case $input in
      [yY] )	gsettings set org.gnome.settings-daemon.plugins.color night-light-enabled true;;
      * )		  gsettings set org.gnome.settings-daemon.plugins.color night-light-enabled false;;
  esac

  echo "Enable minimize and maximize buttons [y/n]?"
  read input
  case $input in
      [yY] )	gsettings set org.gnome.desktop.wm.preferences button-layout 'appmenu:minimize,maximize,close';;
      * )		  gsettings set org.gnome.desktop.wm.preferences button-layout 'appmenu:close';;
  esac

  echo "Enable neo2 keyboard layout [y/n]?"
  read input
  case $input in
      [yY] )	gsettings set org.gnome.desktop.input-sources sources "[('xkb', 'de+neo'), ('xkb', 'de')]";;
      * )		  gsettings set org.gnome.desktop.input-sources sources "[('xkb', 'de')]";;
  esac


  #settings for nautilus
  gsettings set org.gtk.Settings.FileChooser sort-directories-first true
  gsettings set org.gnome.nautilus.list-view use-tree-view true
  gsettings set org.gnome.nautilus.preferences show-create-link true
  gsettings set org.gnome.nautilus.icon-view  captions  "['size', 'permissions', 'none']"
  gsettings set org.gnome.nautilus.preferences executable-text-activation "'ask'"

  #settings for system processes
  gsettings set org.gnome.gnome-system-monitor show-whose-processes "'active'"
  gsettings set org.gnome.gnome-system-monitor show-dependencies true

  #turning off Fedora Logo
  gsettings set org.gnome.shell enabled-extensions "[]"
  sleep 1
}

#setting up importand keybindings for the gnome desktop
function keybindings() {
  if [[ $debug == false ]];
    then
      clear
  fi
  echo "###################################################"
  echo "Setting importand keybindings"
  echo "###################################################"
  echo
    gsettings set org.gnome.desktop.wm.keybindings close "['<Super>q']"
    gsettings set org.gnome.desktop.wm.keybindings toggle-on-all-workspaces "['<Primary><Shift>a']"
    gsettings set org.gnome.desktop.wm.keybindings toggle-fullscreen "['F11']"
    gsettings set org.gnome.desktop.wm.keybindings move-to-workspace-left "['<Primary><Super>Left']"
    gsettings set org.gnome.desktop.wm.keybindings move-to-workspace-right "['<Primary><Super>Right']"
    gsettings set org.gnome.desktop.wm.keybindings show-desktop "['<Super>d']"
    gsettings set org.gnome.settings-daemon.plugins.media-keys screenshot "['<Super>Print']"
    gsettings set org.gnome.settings-daemon.plugins.media-keys email "['<Super>e']"
    gsettings set org.gnome.settings-daemon.plugins.media-keys control-center "['<Super>i']"
    gsettings set org.gnome.settings-daemon.plugins.media-keys home "['<Super>f']"
    gsettings set org.gnome.settings-daemon.plugins.media-keys play "['<Primary>space']"

  #Custom keybindings
    gsettings set org.gnome.settings-daemon.plugins.media-keys custom-keybindings "['/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/', '/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom1/', '/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom2/', '/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom3/']"
    gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/ name "Terminal"
    gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/ command "gnome-terminal"
    gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/ binding "<Super>t"

    gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom1/ name "Tor-Browser"
    gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom1/ command "flatpak run com.github.micahflee.torbrowser-launcher"
    gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom1/ binding "<Super>b"

    gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom2/ name "Atom"
    gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom2/ command "flatpak run io.atom.Atom"
    gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom2/ binding "<Super>c"

    gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom3/ name "KeePassXC"
    gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom3/ command "flatpak run org.keepassxc.KeePassXC"
    gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom3/ binding "<Super>k"
  sleep 1
}

function shutdown() {
  if [[ $debug == false ]];
    then
      clear
      for i in 10 9 8 7 6 5 4 3 2 1 ; do
        echo "###################################################"
        echo "###################################################"
        echo "###################################################"
        echo "###################################################"
        echo "###################################################"
        echo
        echo "       The computer will reboot in " $i "        "
        echo "       Press Ctrl (Strg) + C to cancel           "
        echo
        echo "###################################################"
        echo "###################################################"
        echo "###################################################"
        echo "###################################################"
        echo "###################################################"
        sleep 1
        clear
      done
      systemctl reboot
    else
      echo "###################################################"
      echo "Done. You should restart your computer with systemctl reboot now"
      echo "###################################################"
      echo
  fi
}

function focalboard() {
  if [[ $debug == false ]];
    then
      clear
  fi
  echo "###################################################"
  echo "Installing Focalboard"
  echo "###################################################"
  echo
  wget https://github.com/mattermost/focalboard/releases/download/v0.6.5/focalboard-linux.tar.gz
  tar -xvf focalboard-linux.tar.gz
  mv focalboard-app $home/.local/share/
  rm -r focalboard-linux.tar.gz

  mkdir $home/.local/share/icons
  mkdir $home/Dokumente/.focalboard
  cp  $home/linux-postinstallation-scripts/focalboard/focalboard.svg $home/.local/share/icons/
  echo "#!/bin/bash" > $home/.local/share/startfocal.sh
  echo "cd $home/.local/share/focalboard-app" >> $home/.local/share/startfocal.sh
  echo "./focalboard-app" >> $home/.local/share/startfocal.sh
  chmod +x  $home/.local/share/startfocal.sh
  echo "[Desktop Entry]" > $home/.local/share/applications/focalboard.desktop
  echo "Name=Focalboard" >> $home/.local/share/applications/focalboard.desktop
  echo "Type=Application" >> $home/.local/share/applications/focalboard.desktop
  echo "Exec=$home/.local/share/startfocal.sh" >> $home/.local/share/applications/focalboard.desktop
  echo "Icon=$home/.local/share/icons/focalboard.svg" >> $home/.local/share/applications/focalboard.desktop
  echo "Terminal=false" >> $home/.local/share/applications/focalboard.desktop
  cp  $home/linux-postinstallation-scripts/focalboard/.focalcopy.sh $home
  chmod +x .focalcopy.sh
  rpm-ostree install cronie
  if [[ $debug == false ]];
    then
      clear
  fi
  echo "###################################################"
  echo "Focalboard is succesfully installed. But to call my copyscript u have to enter */1 * * * * $home/.focalcopy.sh in crontab -e "
  echo "###################################################"
  echo
  sleep 2
}

#function to set up the debug mode = never clearing the terminal
function debugMode() {
  echo "###################################################"
  echo "Do you want to use debug mode [y/n]?"
  echo "###################################################"
  echo
  read input
  case $input in
    [yY] )	echo
            echo "###################################################"
            echo "Setting debug mode"
            echo "###################################################"
            debug=true
            sleep 1;;
    * )     echo
            echo "###################################################"
            echo "Debug mode is not gonna be used"
            echo "###################################################"
            sleep 1
  esac
}

function aliasse() {
    echo "alias rm='mv -t ~/.local/share/Trash/files'" >> .bashrc
    echo "alias upgrade='rpm-ostree upgrade && flatpak update -y'" >> .bashrc
}

function deleteFlatpaks() {
flatpak list --columns=application > apps.txt
file='apps.txt'
exec 4<$file
clear
echo "###################################################"
echo "Deleting Software you don't need"
echo "###################################################"
echo

while read -r -u4 t ; do
  echo "###################################################"
  echo "Do you want to remove $t [y/n]?"
  echo "###################################################"
  echo
  read input
  case $input in
    [yY] )	echo
            echo "###################################################"
            echo "Removing Focalboard"
            echo "###################################################"
            flatpak remove $t -y
            echo
            sleep 1;;
    * )     echo
            echo "###################################################"
            echo " $t is not gonna be removed!"
            echo "###################################################"
            sleep 1
  esac
done
rm apps.txt
}


#Start
#Downloading required files
clear
cd $home
debugMode
hostname
deleteFlatpaks
gnomeSettings
keybindings
updates

if [[ $debug == false ]];
  then
    clear
fi
#Adding Flathub Repository
echo "###################################################"
echo "Adding Flathub Repository"
echo "###################################################"
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
flatpak update -y
sleep 1

flatpakInstall Gnome-Powerstatistics org.gnome.PowerStats
flatpakInstall Video org.gnome.Totem
flatpakInstall Extensions org.gnome.Extensions
flatpakInstall Seahorse org.gnome.seahorse.Application
flatpakInstall Firmware org.gnome.Firmware
flatpakInstall Flatseal com.github.tchx84.Flatseal
flatpakInstall "Kooha (Screenrecorder)" io.github.seadve.Kooha
flatpakInstall LibreOffice org.libreoffice.LibreOffice
flatpakInstall Tor com.github.micahflee.torbrowser-launcher
flatpakInstall KeePassXC org.keepassxc.KeePassXC






flatpakInstallAction Nextcloud-Desktop com.nextcloud.desktopclient.nextcloud
flatpakInstallAction Apostrophe org.gnome.gitlab.somas.Apostrophe
flatpakInstallAction Chrome com.github.Eloston.UngoogledChromium
flatpakInstallAction "Solanum (Pomodore App)" org.gnome.Solanum
flatpakInstallAction Atom io.atom.Atom
flatpakInstallAction Evolution org.gnome.Evolution
flatpakInstallAction Pika-Backup org.gnome.World.PikaBackup
flatpakInstallAction Hashbrown dev.geopjr.Hashbrown
flatpakInstallAction Metadaten-Cleaner fr.romainvigier.MetadataCleaner
flatpakInstallAction Popsicle com.system76.Popsicle
flatpakInstallAction Gnome-Web org.gnome.Epiphany
flatpakInstallAction Lollypop org.gnome.Lollypop
flatpakInstallAction Spotify dev.alextren.Spot
flatpakInstallAction EasyTAG org.gnome.EasyTAG
flatpakInstallAction VLC org.videolan.VLC
flatpakInstallAction Shotwell org.gnome.Shotwell
flatpakInstallAction Element im.riot.Riot
flatpakInstallAction Signal org.signal.Signal
flatpakInstallAction "Tootle (Mastodon App)" com.github.bleakgrey.tootle
flatpakInstallAction Microsoft-Teams com.microsoft.Teams
flatpakInstallAction Remmina org.remmina.Remmina
flatpakInstallAction TeamSpeak com.teamspeak.TeamSpeak
flatpakInstallAction Anki net.ankiweb.Anki
flatpakInstallAction Drawing com.github.maoschanz.drawing
flatpakInstallAction "Curtail (Compress images)" com.github.huluti.Curtail
flatpakInstallAction "Dialect (Translator)" com.github.gi_lom.dialect
flatpakInstallAction "Fragments (Torrent Downloader)" de.haeckerfelix.Fragments
flatpakInstallAction Markets com.bitstower.Markets
flatpakInstallAction AnyDesk com.anydesk.Anydesk
flatpakInstallAction Nitrokey com.nitrokey.nitrokey-app

rpmInstall KeePass keepass
rpmInstall GParted gparted
firefox
if [[ $debug == false ]];
  then
    clear
fi
echo "###################################################"
echo "Do you want to install Focalboard[y/n]?"
echo "###################################################"
echo
read input
case $input in
  [yY] )	echo
          echo "###################################################"
          echo "Installing Focalboard"
          echo "###################################################"
          focalboard
          echo
          sleep 1;;
  * )     echo
          echo "###################################################"
          echo " Focalboard is not gonna be installed!"
          echo "###################################################"
          sleep 1
esac
aliasse

sudo chmod +x linux-postinstallation-scripts/fedora/fedoraSilverblue/fedorasilverblueroot.sh
sudo linux-postinstallation-scripts/fedora/fedoraSilverblue/fedorasilverblueroot.sh
shutdown
