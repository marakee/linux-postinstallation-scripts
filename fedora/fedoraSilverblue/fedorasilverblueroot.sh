#!/bin/bash
##### It must be run as root or with sudo privileges

#Setting user to the actual user and not to root
user=$SUDO_USER
#setting the path to the user directory
home=$(getent passwd $SUDO_USER | cut -d: -f6)
#root detection
function rootDetection() {
	if [[ $EUID -ne 0 ]]
		then
			echo "###################################################"
	  	echo "Führe dieses Script bitte als root aus!" 1>&2
	  	echo "Beenden des Scriptes..."
			echo "###################################################"
	  	echo
	  	exit 1
	fi
}

rootDetection
rm -r linux-postinstallation-scripts/
