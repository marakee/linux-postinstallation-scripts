#!/bin/bash
FILENAMEFOCAL="/home/$USER/.local/share/focalboard-app/focalboard.db"
FILENAMENEXT="/home/$USER/Dokumente/.focalboard/focalboard.db"
SERVICE="focalboard-app"
CURTIME=$(date +%s)
FILETIMEFOCAL=$(stat $FILENAMEFOCAL -c %Y)
FILETIMENEXT=$(stat $FILENAMENEXT -c %Y)
TIMEDIFFFOCAL=$(expr $FILETIMEFOCAL - $FILETIMENEXT)
TIMEDIFFNEXT=$(expr $FILETIMENEXT - $FILETIMEFOCAL)

if !(pgrep -x "$SERVICE" > /dev/null)
then
  if [ $TIMEDIFFNEXT -gt 1 ];then
        rm $HOME/.local/share/focalboard-app/focalboard.db
        cp -p $HOME/Dokumente/.focalboard/focalboard.db $HOME/.local/share/focalboard-app/focalboard.db
  fi

  if [ $TIMEDIFFFOCAL -gt 1 ];then
        rm $HOME/Dokumente/.focalboard/focalboard.db
        cp -p $HOME/.local/share/focalboard-app/focalboard.db $HOME/Dokumente/.focalboard/focalboard.db
  fi
fi
